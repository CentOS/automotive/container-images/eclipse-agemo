# Eclipse Agemo

Project URL: https://github.com/eclipse-chariott/agemo

## Building and Pulling

Build the image by running:

```
podman build -t localhost/eclipse-agemo:latest .
```

You can also pull it from quay.io instead of building it yourself:

```
podman pull quay.io/centos-sig-automotive/eclipse-agemo:latest
```

## Running

Agemo depends on three services:

* Eclipse Chariott Server
* Eclipse Chariott Service Discovery
* Eclipse Mosquitto

Those services need to be reachable within the same network that Agemo is running on.

The protobuf import path located at `/usr/local/share/proto/agemo`.

Running Chariott:

```sh
podman run \
-d \
--network host \
quay.io/centos-sig-automotive/eclipse-chariott:latest \
chariott
```

```sh
podman run \
-d \
--network host \
quay.io/centos-sig-automotive/eclipse-chariott:latest \
chariott-service-discobery
```


Running Mosquitto:

```
podman run \
-d \
--network host \
quay.io/centos-sig-automotive/eclipse-mosquitto:latest \
mosquitto
```

Running Agemo:

```sh
podmam run \
-d \
--network host \
quay.io/centos-sig-automotive/eclipse-agemo:latest \
/usr/local/bin/agemo-pub-sub-service
```
