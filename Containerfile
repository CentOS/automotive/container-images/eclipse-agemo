ARG CONTAINER_IMAGE=registry.fedoraproject.org/fedora
ARG CONTAINER_TAG=41

FROM ${CONTAINER_IMAGE}:${CONTAINER_TAG} as builder

ARG GIT_REPO_URL=https://github.com/eclipse-chariott/Agemo.git
ARG GIT_REPO_REFS=0.1.0
ARG BUILD_TYPE=release

RUN dnf install -y \
git \
gcc \
cmake \
rust \
cargo \
openssl-devel \
protobuf-devel \
protobuf-compiler \
perl-FindBin \
perl-IPC-Cmd \
perl-File-Compare \
perl-File-Copy

RUN mkdir -p /tmp/agemo /tmp/build/{bin,proto} && \
git clone ${GIT_REPO_URL} /tmp/agemo

WORKDIR /tmp/agemo

RUN git reset --hard ${GIT_REPO_REFS} && \
git submodule update --init

RUN cargo build --${BUILD_TYPE} && \
cp $(find target/${BUILD_TYPE} -maxdepth 1 -perm -111 -type f) /tmp/build/bin && \
cp -R proto/* /tmp/build/proto/

FROM ${CONTAINER_IMAGE}:${CONTAINER_TAG}

ENV AGEMO_HOME=/etc/agemo
ENV AGEMO_PROTO_DIR=/usr/local/share/proto/agemo/

COPY files/ /

COPY --from=builder /tmp/build/bin/* /usr/local/bin/
COPY --from=builder /tmp/build/proto/ ${AGEMO_PROTO_DIR}/
